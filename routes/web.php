<?php

use Illuminate\Support\Facades\Route;
use App\Box;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// getting the view back from BoxesController's index method through load method
Route::get('/', 'BoxesController@load');

// validating the incoming post datas
Route::post('/postRequest', 'BoxesController@postValidation');
