require('./bootstrap');

$(function() {

    // looping through all elements of the arrays
    leftSide.forEach(boxToDropZone1);
    rightSide.forEach(boxToDropZone2);

    // making <p> elements and giving them the id's from the array in (order of position)
    function boxToDropZone1(boxId)
    {
        var box = $('<p id="' + boxId + '" class="box"></p>').text(boxId);
        $('#dropZone1').append(box);
    }

    // making <p> elements and giving them the id's from the array in (order of position)
    function boxToDropZone2(boxId)
    {
        var box = $('<p id="' + boxId + '" class="box"></p>').text(boxId);
        $('#dropZone2').append(box);
    }

    /*-------------
    DRAGGABLE LOGIC
    -------------*/

    // dragged object returns to original position
    $(".box").draggable(
    {
        // always be on top
        zIndex: 2,
        revert: function (event, ui)
        {
            $("uiDraggable").originalPosition = {top: 0, left: 0};
            return !event;
        }
    });

    // append the dragged object to a drop zone (else it returns to the original position)
    $(".dropZone").droppable(
    {
        // only accept box class
        accept: ".box",

        // when drag is over
        // getting the dropped item into a variable and appending to the div where it gets dropped
        drop: function (event, ui)
        {
            var droppedItem = $(ui.draggable);
            $(droppedItem).detach().css({top: 0, left: 0}).appendTo(this);
            $(this).attr('id') == "dropZone1" ? $side = "left" : $side = "right";

            // send a post request to update the boxes
            $.ajax({
                url: "postRequest",
                method: "POST",
                data:
                    {
                        id: $(droppedItem).attr('id'),
                        side: $side
                    },
                datatype: "json",
                headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')}
            });
        }
    });

    /*--------------------
    END OF DRAGGABLE LOGIC
    --------------------*/
});
