@extends('layouts.app')

<!-- script -->
@section('script')

    <script>
        // js arrays for the sides
        var leftSide = [];
        var rightSide = [];

        // filling up left side array in js
        @foreach($leftSide as $id)
            leftSide.push({{$id}});
        @endforeach

        // filling up right side array in js
        @foreach($rightSide as $id)
            rightSide.push({{$id}});
        @endforeach
    </script>

@endsection
<!-- end of script -->

<!-- content -->
@section('content')

    <header>
        <h1>Lorem ipsum dolor sit amet, consectetur</h1>
        <hr>
        <h2>Quia consequuntur magni dolores eos q</h2>
    </header>

    <div class="container">
        <div id="dropZone1" class="dropZone">

        </div>
        <div id="dropZone2" class="dropZone"></div>
    </div>

    <div id="info">
        <i class="fa fa-info-circle"></i>
        <p>Húzza a dobozokat a jobb oldalra.</p>
    </div>

@endsection
<!-- end of content -->

