<?php

use Illuminate\Database\Seeder;

class BoxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // makes N seeds to the database -> php artisan db:seed
        $seedNumber = 9;
        for($i = 1; $i < $seedNumber+1; $i++)
        {
            DB::table('boxes')->insert([

                'side' => 'left',
                'position' => $i

            ]);
        }
    }
}
