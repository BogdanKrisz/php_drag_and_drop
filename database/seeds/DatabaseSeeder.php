<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // makes a record into the boxes table (in this case 9 and can be modified inside BoxesTableSeeder)
        $this->call(BoxesTableSeeder::class);
    }
}
