<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Box;
use function GuzzleHttp\Promise\queue;

class BoxesController extends Controller
{
    // arrays that will contain the boxes sorted by side
    private $leftSide = array();
    private $rightSide = array();

    // loads everything before relocates to index
    public function load()
    {
        $this->loadSides();
        return $this->index($this->leftSide, $this->rightSide);
    }

    // returns the welcome view and gives it the two arrays
    private function index(array $leftSide, array $rightSide)
    {
        return view('welcome', compact('leftSide', 'rightSide'));
    }

    // getting all the boxes from database and filling up the arrays
    // sorted by side
    private function loadSides()
    {
        // getting all the instances of box from db ascending by position
        $boxes = Box::all()->sortBy('position');

        // looping through all boxes and sorting them by side
        foreach($boxes as $box)
        {
            $box->side == "left" ? array_push($this->leftSide, $box->id) : array_push($this->rightSide, $box->id);
        }
    }

    // validating the data from the post request
    public function postValidation(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric|max:11',
            'side' => 'required|max:5'
        ]);

        // after validation calls the update method with the id and side parameters
        $this->update($request->get('id'), $request->get('side'));
    }

    // updating boxes position's
    public function update(int $id, string $side)
    {
        // getting the original side and position of the updated box (before update)
        $originalBox = Box::find($id);
        $oldSide = $originalBox->side;
        $oldPosition = $originalBox->position;

        // getting the number of boxes on the new side (so i can put the moved box to the end)
        $sideBoxes = Box::where('side', "$side")->get();
        $newPosition = $sideBoxes->count()+1;

        // update the moved box's side and position
        $originalBox->side = $side;
        $originalBox->position = $newPosition;
        $originalBox->save();

        // getting all the boxId's after the moved one, by positions
        $boxesToReArrange = Box::where([['side', "$oldSide"], ['position', '>', "$oldPosition"]])->get();

        // update all positions after the moved one by -1
        foreach($boxesToReArrange as $box)
        {
            $box->position -= 1;
            $box->save();
        }
    }
}
