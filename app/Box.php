<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    // disable auto timestamps
    public $timestamps = false;

    // enables mass-assignment
    protected $fillable = [
        'side',
        'position'
    ];
}
